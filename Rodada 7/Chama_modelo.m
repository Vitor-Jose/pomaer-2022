% Codigo para testar os melhores resultados
clear; close all; clc;
% [4.44294004069358 3.38957141212742 0.764431934025687 0.6 8.08990267199045 22.3615207740188 12.6968824838761]
arquivos = {'Otimizacao_josep' 'Otimizacao_LucaV' 'Otimizacao_guerreiro_7'  };
rodada = 'Rodada 7';

n_last = 10;

for tt = 1:n_last
    load(arquivos{1})             % Importando o arquivo de melhores aeronaves
    
    % === Best ===
%     otm = Individuos_otimizados;
     otm = [3.8576 3.9527 0.5609 0.8205 1.5936 20.5572 12.3189];
%     otm(4) = 0.70;
%     otm(5) = 9.5;
%     otm(6) = 24;
    [~,pos] = sort(ind_itermax(:,1));
    disp([pos ind_itermax(pos,:)])
    otm = ind_itermax(pos(tt),2:end);   
    
    cd ..
    [result] = modelo(otm,0,[1 1 1]);
    load('Aeronave.mat');
%     VLManda(geo,flc,sim,2,'-LiftingSurfaces');% So colar na command Window
    cd(rodada)
%     
%     save([arquivos{tt} '_plane'])
%     
    figure(1)
    subplot(1,2,1)
    plot(0:24,stab.coeff_aoa.fit(1).CL(0:24))
    subplot(1,2,2)
    plot(0:24,stab.coeff_aoa.fit(2).CL(0:24))
    title('C_L')
    hold on
    
    figure(2)
    subplot(1,2,1)
    plot(0:20,stab.coeff_aoa.fit(1).CD(0:20))
    subplot(1,2,2)
    plot(0:20,stab.coeff_aoa.fit(2).CD(0:20))
    title('C_D')
    hold on
    
    figure(3)
    subplot(1,2,1)
    plot(0:20,stab.coeff_aoa.fit(1).Cm25(0:20))
    subplot(1,2,2)
    plot(0:20,stab.coeff_aoa.fit(2).Cm25(0:20))
    title('C_m')
    hold on
%     
%     % === Mean ===
% %     I = ind_itermax;               % Trazendo as matrizes de valores 
% %     media = mean(I(:,2:end));
% %     
% %     cd ..
% %     [result] = modelo(media,0,[1 1 1]);
% %     load('Aeronave.mat');
% %     VLManda(geo,flc,sim,2,'-LiftingSurfaces');% So colar na command Window
% %     cd(rodada)
% %     
% %     save([arquivos{tt} '_plane2'])
% %     
% %     figure(1)
% %     subplot(1,2,1)
% %     plot(0:20,stab.coeff_aoa.fit(1).CL(0:20))
% %     subplot(1,2,2)
% %     plot(0:20,stab.coeff_aoa.fit(2).CL(0:20))
% %     title('C_L')
% %     
% %     figure(2)
% %     subplot(1,2,1)
% %     plot(0:20,stab.coeff_aoa.fit(1).CD(0:20))
% %     subplot(1,2,2)
% %     plot(0:20,stab.coeff_aoa.fit(2).CD(0:20))
% %     title('C_D')
% %     
% %     figure(3)
% %     subplot(1,2,1)
% %     plot(0:20,stab.coeff_aoa.fit(1).Cm25(0:20))
% %     subplot(1,2,2)
% %     plot(0:20,stab.coeff_aoa.fit(2).Cm25(0:20))
% %     title('C_m')
end
