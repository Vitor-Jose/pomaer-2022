function [CDl,CDservo,CDb,CDtp] = arrasto_sempolar(geo,flc)
%% Nomenclatura:

% DADOS DE SAiDA:
% CDeb          => Coeficiente de arrasto parasita do boom;
% CDl           => Coeficiente de arrasto parasita da linkagem;
% CDservo       => Coeficiente de arrasto parasita dos servos;
% CDetp         => Coeficiente de arrasto parasita total do trem de pouso;

% DADOS DE ENTRADA:
% Sw            => Area molhada da asa;
% V             => Velocidade de voo;
% p             => Massa especifica do fluido;
% u             => Viscosidade dinamica do fluido;
% Dtubo         => Diametro dos tubos de carbono;
% Dplaca        => Diametro caracteristico da placa;
% Ds            => Diametro caracteristico do servo motor;
% Dl            => Diametro caracteristico da linkagem;
% Dtp_rb        => Diametro caracteristico da roda da bequilha do trem de pouso;
% Dtp_rp        => Diametro caracteristico das rodas principais do trem de pouso;
% Dtp_b         => Diametro caracteristico da barra do trem de pouso;

%% INICIALIZACAO DAS VARIAVEIS:
Sw                  = geo.LiftingSurface.Sw(1);                             % Area Projetada da asa
V                   = flc.Voo;                                              % Velocidade estimada de voo
beta                = flc.aos;                                              % Angulo de derrapagem

p = flc.rho;                                                                % Densidade do fluido 
u = flc.visc;                                                               % Viscosidade dinamica

% Medidas do Boom
% Tubos presentes no Boom
qtd_tuboh	= 17;                                                           % Quantidade de tubos, considerando os na diagonal como horizontais
D_tuboh     = 3e-3;                                                         % Diametro dos tubos de carbono
L_tuboh     = 101.8e-3;                                                     % Comprimento dos tubos

% Lateral Entelada
C_sup_placa = 63e-2;                                                        % Comprimento superior da placa lateral do Boom
H_max_placa = 95e-3;                                                        % Altura maxima da placa lateral do Boom
H_min_placa = 45e-3;
diag_placa  = 145e-3;

% Medidas da Linkagem
qtde_tubol  = 2;                                                           	% Quantidade dos tubos de linkagem
Dlink       = 1.4e-3;                                                      	% Diametro do tubo de linkagem [m]
L_tubol     = 143e-3;                                                       % Comprimento do tubo de linkagem [m]

% Medidas do servo
H_servo     = 19.5e-3;                                                      % Altura do servo
C_servo     = 41.5e-3;                                                      % Comprimento do servo
L_servo     = 38e-3;                                                        % Largura do servo

% Medidas do Trem de pouso (Medidas tomadas de acordo com a visao frontal da aeronave)

% Roda Trem de pouso Principal
E_roda      = 14e-3;                                                       % Espessura de ambas as rodas do trem de pouso principal
D_roda      = 91e-3;                                                       % Di�metro de ambas as rodas do trem de pouso principal

% Roda da Bequilha
E_beq       = 14e-3;                                                       % Espessura da roda da bequilha
D_beq       = 81e-3;                                                       % Di�metro da roda da bequilha

% Barra do Trem de pouso
H_tpb       = 6.2e-3;                                                      % Altura do eixo do trem de pouso principal
L_tpb       = 308e-3;                                                      % Largura do eixo do trem de pouso principal
C_tpb       = 15e-3;                                                       % Comprimento do eixo do trem de pouso principal

%% Coeficientes encontrados na literatura

Cdret       = 1.4;                                                         % Coeficiente de arrasto medio de uma secao retangular considerando L/D igual a 2;
Cdretm      = 2.0;                                                         % Coeficiente de arrasto medio de uma secao retangular considerando L/D igual a 1;
Cdcirc      = 1.17;                                                        % Coeficiente de arrasto me�dio de uma secao circular
Cdr         = 0.3;                                                         % Rodas sem carenagens; {Mudar para Cdr=0.15 - se colocar carenagens.}



%% Diametros Hidraulicos para calculo de Reynolds

Dplaca = 2*C_sup_placa*D_tuboh/(2*(C_sup_placa+D_tuboh));
Dl = Dlink/2;
Ds = 2*H_servo*L_servo/(2*(H_servo+L_servo));
Dtp_rb = 2*E_beq*D_beq/(2*(E_beq+D_beq));
Dtp_rp = 2*E_roda*D_roda/(2*(E_roda+D_roda));
Dtp_b = 2*H_tpb*L_tpb/(2*(H_tpb+L_tpb));

%% CALCULO DE REYNOLDS:

Ret = V*D_tuboh*p/u;                                 % Re dos tubos de carbono do boom;
Rep = V*Dplaca*p/u;                               % Re da placa enteleada da lateral do boom;
Rel = V*Dl*p/u;                                          % Re da lincagem;
Res = V*Ds*p/u;                                        % Re do servo motor;
Retp_rb = V*Dtp_rb*p/u;                       % Re da roda da bequilha do trem de pouso;
Retp_rp = V*Dtp_rp*p/u;                       % Re das rodas principais do trem de pouso;
Retp_b = V*Dtp_b*p/u;                           % Re da barra do trem de pouso;

%% CALCULO DOS COEFICIENTES DE FRICAO ENTRE O AR E A SUPERFICIE DE CONTATO DE CADA COMPONENTE:

Cft = .455./(log10(Ret)).^2.58;                                             % Coef. Friccao dos tubos de carbono do Boom
Cfp = .455./(log10(Rep)).^2.58;                                             % Coef. Friccao da placa lateral do Boom
Cfl = .455./(log10(Rel)).^2.58;                                             % Coef. Friccao do tubo de linkagem
Cfs = .455./(log10(Res)).^2.58;                                             % Coef. Friccao do servo localizado no Boom
Cftp_rb = .455./(log10(Retp_rb)).^2.58;                                     % Coef. Friccao da roda da bequilha
Cftp_rp = .455./(log10(Retp_rp)).^2.58;                                     % Coef. Friccao da roda do trem de pouso principal
Cftp_b = .455./(log10(Retp_b)).^2.58;                                       % Coef. Friccao do eixo do trem de pouso principal

%% C�LCULO DO ARRASTO PARASITA DA LINKAGEM

% C�lculo de arrasto de forma (cilindro longo)
S_front_tubol = pi*( (Dlink/2)^2);                  %�rea frontal dos tubos
S_tubol       = Dlink*L_tubol;                      %�rea lateral dos tubos

CDl = qtde_tubol*(S_front_tubol*(Cdcirc + Cfl)+ S_tubol*Cfl)/Sw;  % Coeficiente de arrasto parasita da linkagem

%% C�LCULO DO ARRASTO PARASITA DO SERVO MOTOR

S_front_servo =  H_servo * L_servo;                   % �rea frontal do servo
S_lat_servo   =  H_servo * C_servo;
S_sup_servo   =  L_servo * C_servo;
S_servo       = (S_sup_servo + S_lat_servo)*2 + S_front_servo; 

CDservo = (Cfs*S_servo + Cdret*S_front_servo)/Sw;  % Coeficiente de arrasto parasita do servo

%% C�LCULO DO ARRASTO PARASITA DE TUBOS

% area frontal
S_tuboh  = qtd_tuboh*pi*D_tuboh*L_tuboh;             % �rea molhada dos tubos horizontais
S_lat         = sqrt(diag_placa^2 - H_max_placa^2)*H_max_placa + ...
    (C_sup_placa - sqrt(diag_placa^2 - H_max_placa^2))* (H_min_placa+H_max_placa);  % �rea frontal das laterais

% coeficientes da lateral
rel_lat	= C_sup_placa/H_max_placa;                        % rela��o utilizada para calcular o Cd de um paralelep�pedo

if rel_lat < 1
    disp('rela��o fora dos dados interados')
    keyboard
    
else
    if 1 <= rel_lat && rel_lat <= 18
        Cdobliquo = 0.01615*rel_lat + 1.117;     % intera��o para o calculo do Cd de um paralelep�pedo
    else
        Cdobliquo = 2.01;
    end
end

% Calculo do CD parasita do boom
CDb = ((cosd(beta)*Cdcirc*S_tuboh + Cft*S_tuboh) + (sind(beta)*Cdobliquo*S_lat + Cfp*S_lat))/Sw;  % Cd parasita do Boom


%% C�LCULO DO ARRASTO PARASITA DO TREM DE POUSO:
rel_lat_roda = 1; % Raz�o entre o comprimento e o di�metro
Cdobliquo_roda = 0.01615*rel_lat_roda + 1.117;

% Soma das contribui��es das rodas, lembrando que o trem de pouso principal tem 2 rodas:
% CDerodas_p	= ((Cftp_rp+Cdobliquo_roda)*2*pi*(D_roda/2)^2 + Cdr*pi*D_roda*E_roda)/Sw;
CDerodas_p	= Cdr*pi*D_roda*E_roda/Sw;
CDerodas_b  = Cdr*pi*D_beq*E_beq/Sw;
% ((Cftp_rb+Cdobliquo_roda)*2*pi*(D_beq/2)^2 + Cdr*pi*D_beq*E_beq)/Sw;
CDerodas = 2*CDerodas_p + CDerodas_b;

% Arrasto parasita da estrutura do trem de pouso:
Stpp	= L_tpb*H_tpb+2*C_tpb*L_tpb;	% �rea do trem de pouso principal [m�].
Cdtp	= Cdret;		    % Considerando a estrutura aerodinamica(carenada ou perfilada).
CDest	= (Cftp_b + Cdtp + Cdobliquo)*(Stpp/Sw);

% Arrasto Parasita Total do trem de pouso:
CDtp	= CDerodas + CDest;

end

%% REFERENCIAS:

% Equacoes e tabelas conseguidos em:
% http://faculty.dwc.edu/sadraey/Chapter%203.%20Drag%20Force%20and%20its%20Coefficient.pdf
% Coeficiente de arrasto medio de uma secao retangular: file:///C:/Users/amand/Downloads/Aerodynamics%20and%20Flight%20Mechanics.pdf;
% Coeficiente de arrasto medio de uma secao circular: https://avionicsengineering.files.wordpress.com/2016/11/john-d-anderson-jr-fundamentals-of-aerodynamics.pdf,p�gina 318; http://professor.ufabc.edu.br/~roldao.rocha/wordpress/wp-content/uploads/Est_Docente_Q3-2017.pdf
% Calculo do diametro equivalente realizado segundo Barros. [pag. 4.4.3-2]
