function [ard,MAC, penal] = estol(geo, flc, sim)
%ESTOL Summary of this function goes here
%   Calculo do alha estol (ard.alpha_estol), a deflexao maxima do profundor
%   (ard.deltae) e alguns parametros aerodinamicos da geometria
%    Autor: Vitor Jos� Meyer Maffei

penal = 0;                                                                 %Defini��o de uma penalidade caso a aeronave n�o apresente um c�lculo correto pelo MACACO
% CALCULO DO ESTOL DA AERONAVE
flc.Voo = 12.5;                                                              % [m/s] condicao de velocidade para os calculos
flc.aoa = [17.5 18];                                                       % [deg] intervalo de avaliacao do angulo de ataque
% INICIALIZA��O
results = VLManda(geo, flc, sim, 0, '-LiftingSurfaces');
guard = zeros(16,1);
guard(7) = results(3).Coeffs(1).CL;
guard(8) = results(3).Coeffs(2).CL;
if guard(8) > guard(7)
    CLmax       = guard(8);
    alfa_max    = 18;
    for i = 1:8
        flc.aoa = 18 + 0.5*i;
        results = VLManda(geo, flc, sim, 0,'-LiftingSurfaces');
        guard(8+i)= results(3).Coeffs.CL;
        if guard(8+i) > guard(7+i)
        CLmax      = guard(8+i);
        alfa_max    = flc.aoa;
        elseif guard(8+i)<= guard(7+i)
        CLmax      = guard(7+i);
        alfa_max    = flc.aoa - 0.5;
        break
        end
    end
else
    CLmax       = guard(7);
    alfa_max    = flc.aoa(1);
    for i = 1:6
        flc.aoa = 17.5 - 0.5*i;
        results = VLManda(geo, flc, sim, 0, '-LiftingSurfaces');
        guard(7-i) = results(3).Coeffs.CL;
        if guard(8-i) <= guard(7-i)
        CLmax      = guard(7-i);
        alfa_max    = flc.aoa;
        elseif guard(8-i) > guard(7-i)
        CLmax      = guard(8-i);
        alfa_max    = flc.aoa + 0.5;
        break
        end
    end
end
ard.CL_max      = CLmax;                                                   % Maximo valor de CL
ard.alpha_estol = alfa_max;  

if isnan(CLmax)
    penal = 1;
    disp('Erro no c�lculo de CL m�ximo!');
end
% Angulo de rotacao maximo (usado para decolagem da aeronave)
                                              
 ard.AreaAsaINF  = results(1).Coeffs.Sref;
 ard.AreaEH      = results(2).Coeffs.Sref;
 MAC(1,1)        = results(1).MAC;



% CALCULO DO ESTOL DA EMPENAGEM HORIZONTAL
%     flctrim = flc;                                                              % [m/s] condicao de velocidade para os calculos
%     flctrim.aoa = [-17:-14];                                                       % [deg] intervalo de avaliacao do angulo de ataque
%     results2 = VLManda(geo, flctrim, sim, 0, '-EH');
%     for i = 2:length(flctrim.aoa)
%         if results2(1).Coeffs(i).CL < results2(1).Coeffs(i-1).CL
%             alpha_max = flctrim.aoa(i);
%         else
%             alpha_max = flctrim.aoa(i -1);
%             break
%         end     
%     end
%     ard.estol_eh       = alpha_max;                                             % angulo de estol da EH

%flctrim = flc;
% Definicao de uma nova flight conditions para avaliacao da EH
% flctrim.aoa = (-9:-0.5:-20);                                                % Valores de alfa (flight condition) para analises da EH
% 
% % Chamada do VLM para o profundor - variacao de alpha (flctrim.aoa) serve como a variacao de deflexao do profundor
% results2 = VLManda(geo, flctrim, sim, 0, '-EH');                                  % Retorna os coeficientes aerodinamicos para a Empenagem (sem influencia das asas)
% for i = 2:length(flctrim.aoa)
%     if results2(1).Coeffs(i).CL < results2(1).Coeffs(i-1).CL
%         alpha_max = flctrim.aoa(i);
%     elseif results2(1).Coeffs(i).CL >= results2(1).Coeffs(i-1).CL
%         alpha_max = flctrim.aoa(i-1);
%         break
%     end     
% end
% 
ard.estol_eh = -15;
end