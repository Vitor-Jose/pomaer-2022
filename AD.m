clc;clear;
AP = 636*3.281;
out = [2010	19.53	750	2.97
2011	20.91	754	3.11
2012	22.27	711	3.30
2013	20.07	760	3.07
2014	23.90	754	3.84
2015	23.09	764	2.79
2016	21.52	768	3.16
2017	21.85	777	3.12
2018	20.82	795	2.70
2019	22.54	776	2.78];

nov = [2010	21.43	734	2.76
2011	20.07	692	2.96
2012	21.19	742	2.36
2013	21.78	737	3.22
2014	24.08	741	2.18
2015	23.12	758	2.02
2016	21.40	737	2.71
2017	20.71	753	2.70
2018	21.54	770	2.72
2019	22.13	753	2.74];

dez = [2010	23.26	755	1.86
2011	22.27	755	2.59
2012	24.21	779	1.95
2013	23.24	761	2.30
2014	25.70	768	2.78
2015	23.89	789	2.07
2016	23.25	773	2.58
2017	22.54	776	2.02
2018	23.60	783	2.94
2019	23.03	797	2.37];

jan = [2010	23.03	767	1.86
2011	23.48	770	1.91
2012	21.30	768	1.90
2013	21.94	780	2.22
2014	24.59	758	2.09
2015	27.39	768	2.36
2016	23.74	762	2.20
2017	23.35	782	2.03
2018	22.97	790	2.12
2019	24.94	797	2.08];

fev = [2010	24.58	679	2.42
2011	24.11	681	2.03
2012	24.37	701	2.05
2013	22.86	698	1.78
2014	25.59	678	2.71
2015	25.28	711	2.11
2016	24.85	712	2.01
2017	24.68	673	2.00
2018	22.80	705	1.95
2019	23.33	733	2.69];

mar = [2010	23.08	766	2.44
2011	22.13	756	1.85
2012	22.56	768	2.15
2013	22.15	775	2.34
2014	23.11	758	1.91
2015	24.34	774	1.71
2016	23.35	777	1.86
2017	22.65	767	2.50
2018	24.04	786	1.55
2019	23.01	794	2.10];

abr = [2010	20.85	730	2.93
2011	21.52	729	2.07
2012	21.31	733	2.17
2013	19.89	738	2.25
2014	22.33	735	2.61
2015	23.38	742	1.94
2016	23.58	735	3.02
2017	20.92	737	2.61
2018	21.54	743	1.81
2019	22.52	749	2.03];

data = [out nov dez jan fev mar abr];

[T, a, P, rho] = atmosisa((AP/3.281));
T = T - 273.15;

Altden = (AP+(100*(data(:,2:4:end)-T)))/3.281;
err = data(:,4:4:end)/2;
X = data(:,1:4:end);
Xx = {'2010','2011','2012','2013', '2014','2015','2016','2017','2018','2019'};

p1 = bar(Altden,'hist');
yticks(100:100:1400)
hold on
model_series = Altden;
model_error  = err;
[ngroups, nbars] = size(model_series);

groupwidth = min(0.8, nbars/(nbars + 1.5));

for i = 1:nbars
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    e = errorbar(x, model_series(:,i), model_error(:,i), 'k', 'linestyle', 'none');
end
grid on
set(gca, 'YGrid', 'on', 'XGrid', 'off')
set(gca,'xticklabel',Xx)
legend('Outubro','Novembro','Dezembro','Janeiro','Fevereiro','Mar�o','Abril','Erro Associado')
title('M�dia de AD - SJC - [Out - Abr]')
xlabel('Anos de Medi��o')
ylabel('AD [m]')
hold off

    


