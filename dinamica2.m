% Desempenho 2022 - POMAER
% Maio de 2022
% ARRUMAR LIMITES
function [mtow] = dinamica2(geo,flc,sim,ard,std)
%% PRE-DEFINICOES 
Vrot    = flc.Vrot;                                                         % Veloocidade de rotacao
Stot    = 58;                                                               % Distancia total da pista
alt     = 0.7;                                                              % Altura da faixa

% arf     = ard.alpha_estol-1;

ProfID = geo.LiftingSurface.surfacenum;                                     % Identidade do profundor
Sref   = geo.LiftingSurface.Sw(1);                                          % Area de referencia
Cref   = geo.MAC;                                                           % Corda de referencia
deflexao0 = geo.LiftingSurface.incidence(ProfID);                           % Deflexao inicial do profundor

CLq = 5.6725;
Cmq = -7.129;
CDparasita = 0.03;

deltaealivio = 0.3*std.range;
deltaeSpeed = -100;
%% VLM - Coeficientes conforme a velocidade
v_reduzido = 2.5:2.5:Vrot;
v_reduzido = v_reduzido + flc.v_vento;

flc.Voo = v_reduzido;                                                     
flc.aoa = 0;

if sim.paralelo
    coeffs = VLMandap(geo,flc,sim,0,'-LiftingSurfaces');
else
    coeffs = VLManda(geo,flc,sim,0,'-LiftingSurfaces');
end

if coeffs(3).penal
    mtow = 5;
    return
end

for j=1:geo.LiftingSurface.surfacenum
    for i = 1:length(coeffs(j).Coeffs)
        coef(j).CL(i) = coeffs(j).Coeffs(i).CL;
        coef(j).CD(i) = coeffs(j).Coeffs(i).CD;
        coef(j).Cm25(i) = coeffs(j).Coeffs(i).Cm25;
    end
    coeff.fit(j).CL = fit([0 flc.Voo]',[coef(j).CL(1) coef(j).CL]','linearinterp');         % Nesse loop e feito o fit dos coeficientes (apenas para simplificar o acesso aos coeficientes)
    coeff.fit(j).CD = fit([0 flc.Voo]',[coef(j).CD(1) coef(j).CD]','linearinterp');
    coeff.fit(j).Cm25 = fit([0 flc.Voo]',[coef(j).Cm25(1) coef(j).Cm25]','linearinterp');
end

coeff_0   = coeff; % Coeficientes conforme a velocidade

%% VLM - Coeficientes conforme o angulo de ataque
coeff_aoa = std.coeff_aoa; % Coeficientes conforme o angulo de ataque

%% Referenciais
% === Apoiada no trem de pouso ===
% 1 - Centros aerodinamicos em relacao ao trem de pouso
CA_tp   = zeros(3,ProfID); 

for aux_s = 1:ProfID
    CA_tp(:,aux_s) = [-std.cg-geo.tp.pos(1)+geo.LiftingSurface.pos(aux_s,1); ...
            0;-geo.cg.pos(3)-geo.tp.pos(3)+geo.LiftingSurface.pos(aux_s,3)];
end 
CAt_tp = CA_tp(:,ProfID);

% 2 - Motor em relacao ao trem de pouso
ENG_tp    = [0;0;geo.mp.pos(3)-geo.tp.pos(3)];

% 3 - Centro de gravidade em relacao ao trem de pouso
CG_tp  = [-geo.tp.pos(1);0;-geo.tp.pos(3)];


% === Com base no CG ===
% 1 - Centros aerodinamicos em relacao ao trem de pouso
CA_cg   = zeros(3,ProfID); 

for aux_s = 1:ProfID
    CA_cg(:,aux_s) = [-std.cg+geo.LiftingSurface.pos(aux_s,1); ...
            0;-geo.cg.pos(3)+geo.LiftingSurface.pos(aux_s,3)];
end 
CAt_cg = CA_cg(:,ProfID);

% 2 - Motor em relacao ao centro de gravidade
ENG_cg    = [0;0;geo.mp.pos(3)];

%% Iteracoes
for m = 11:0.1:20
% for m = 13
% Massa e momento de inercia
Iyy_cg = 477402610e-9;
Iyy_tp = 477402610e-9 + m*(-geo.tp.pos(3))^2;

% Contagem
cont    = 1;

% Tempo
t       = 0;            % Instante de tempo
dt      = 0.02;         % Passo de tempo

% Posicao linear
Sx       = 0;            % Espaco
Vx       = 0;            % Velocidade
ax       = 0;            % Aceleracao

Sz       = 0;
Vz       = 0;
az       = 0;

% Posicao angular
theta   = 0;            % Angulo entre referenciais
thetaum = 0;            % Velocidade angular
thetadois = 0;          % Aceleracao angular

% Demais angulos
alpha   = 0;
gamma   = 0;

% Logico para verificar se ja passou de Vrot
rotacionar = 0;
manter = 0;
passou = 0;
%% Loop
while true
    alpha = theta-gamma;        % Calculo do angulo de ataque 
    Voo   = sqrt((Vx+flc.v_vento)^2+Vz^2);    % Calculo da velocidade do vento
    
   if  alpha > std.arf
        fprintf('Estolou! Estolou! :0\n')
        break
   elseif alpha >= std.arf-1
       manter = 1;
    end
    
    Ealpha = [cosd(alpha) 0 -sind(alpha); 0 1 0 ; sind(alpha) 0 cosd(alpha)];  % Matriz de rotacao com alpha (Lift Surfaces e Finercia)
    Etheta = [cosd(theta) 0 -sind(theta); 0 1 0 ; sind(theta) 0 cosd(theta)];  % Matriz de rotacao com theta (Peso) 
    
    % Caso chegue na velocidade de rotacao
    if manter
        deltae = ard.estoleh+deltaealivio; 
        CLprof = std.CLdeltae*(deltae-deflexao0);
    elseif Voo >= Vrot || rotacionar % && alpha <= std.arf-1.5)
        rotacionar = 1;
        deltae = max(ard.estoleh, deltae + deltaeSpeed*dt); 
        CLprof = std.CLdeltae*(deltae-deflexao0);
    else
        deltae = 0;
        CLprof = 0;
    end
    
% ============================= NA TERRA ==================================
    if Sz == 0 
         
    % === Caso esteja rotacionando ===
    if rotacionar
        
    % Forcas & Momentos (Pitch Rate - Q)
    Lq = 0.25*CLq*thetaum*flc.rho*Voo*Sref;
    Mq = 0.25*Cmq*thetaum*flc.rho*Voo*Cref*Sref; 
    
        % 1 - Resultantes nos centros aerodinamicos
        for j = 1:ProfID-1
            % Asas
            Lw(:,j) = [0; 0; 0.5*flc.rho*(Voo^2)*Sref*coeff_aoa.fit(j).CL(alpha) + Lq];
            Dw(:,j) = [0.5*flc.rho*(Voo^2)*Sref*coeff_aoa.fit(j).CD(alpha);0 ; 0];
            Mw(:,j) = [0; 0.5*flc.rho*(Voo^2)*Sref*coeff_aoa.fit(j).Cm25(alpha)*Cref + Mq; 0];
    
            M(:,j) = cross(CA_tp(:,j),(Ealpha*(Lw(:,j)+Dw(:,j))));
        end
            % Profundor
            Lt = [0; 0; 0.5*flc.rho*(Voo^2)*Sref*(coeff_aoa.fit(ProfID).CL(alpha)+CLprof)]; 
            Dt = [0.5*flc.rho*(Voo^2)*Sref*coeff_aoa.fit(ProfID).CD(alpha); 0; 0];
            Mt = [0; 0.5*flc.rho*(Voo^2)*Sref*coeff_aoa.fit(ProfID).Cm25(alpha)*Cref; 0];  
    
    % === Caso nao esteja rotacionando ===        
    else
        
        % 1 - Resultantes nos centros aerodinamicos
        for j = 1:ProfID-1
            % Asas
            Lw(:,j) = [0; 0; 0.5*flc.rho*(Voo^2)*Sref*coeff_0.fit(j).CL(Voo)];
            Dw(:,j) = [0.5*flc.rho*(Voo^2)*Sref*coeff_0.fit(j).CD(Voo);0 ; 0];
            Mw(:,j) = [0; 0.5*flc.rho*(Voo^2)*Sref*coeff_0.fit(j).Cm25(Voo)*Cref; 0];
    
            M(:,j) = cross(CA_tp(:,j),(Ealpha*(Lw(:,j)+Dw(:,j))));
        end
            % Profundor
            Lt = [0; 0; 0.5*flc.rho*(Voo^2)*Sref*(coeff_0.fit(ProfID).CL(Voo))];
            Dt = [0.5*flc.rho*(Voo^2)*Sref*coeff_0.fit(ProfID).CD(Voo); 0; 0];
            Mt = [0; 0.5*flc.rho*(Voo^2)*Sref*coeff_0.fit(ProfID).Cm25(Voo)*Cref; 0];     
    end
    % ==================================== 
    Mprof = cross(CAt_tp,(Ealpha*(Lt+Dt)))+Mt;
    
    Dparasita = 0.5*flc.rho*(Voo^2)*0.85*CDparasita;
    
    % 2 - Resultantes no motor
    T = [-empuxo(Voo,geo.mp.conjunto,flc.rho);0;0];
    Mmotor = cross(ENG_tp,T);
    
    % === Forcas verticais ===
    % Considerando decomposicao de sustentacao e arrasto
    V = (sum(Lw(3,:))+Lt(3))*cosd(gamma)+T(1)*sind(theta)-(sum(Dw(1,:))+Dt(1)+Dparasita)*sind(gamma)-m*flc.g; % Soma das for�as verticais
    
    if V <= 0 && Sz <= 0 % Forca normal devido contato com o solo
        N = - V;
        Sz = 0; Vz = 0;
        
    else                 % Sem forca normal por nao encostar no solo e aceleracao vertical
        N = 0;
        az = V/m;
        Sz = max([Sz + Vz*dt + 0.5*az*dt^2 0]);
        Vz = max([Vz + az*dt 0]);  
    end
    
    % === Forcas horizontais ===
    ax = -(T(1)*cosd(theta)+(sum(Dw(1,:))+Dt(1)+Dparasita)*cosd(gamma)+(sum(Lw(3,:))+Lt(3))*sind(gamma)+N*flc.mi)/m;
    
    Sx = Sx + Vx*dt + 0.5*ax*dt^2;
    Vx = Vx + ax*dt;
    
    % 3 - Resultantes no centro de gravidade
    W = [0;0;-m*flc.g];         % Peso
    Finercia = [ax*m;0;0];       % Forca inercial
    Mcg = cross(CG_tp,(Ealpha*Finercia)+(Etheta*W));
    
    % === Momento total ===
    Mtotal = sum(M(2,:))+sum(Mw(2,:))+Mprof(2)+Mmotor(2)+Mcg(2);
    
    if  Mtotal < 0 && theta == 0
        Mtotal = 0;
    end
    
    if Sx >= Stot
        fprintf('Nao subiu com %4.1f kg\n',m);
        break        
    end
    
    gamma = atand((Vz/Vx));

    theta = theta*pi/180;           % Trasnformacao de graus para radianos
    
    thetadois = Mtotal/Iyy_tp;                          % Aceleracao angular
    theta = theta + thetaum*dt + thetadois*dt^2/2;      % Posicao angular
    thetaum = thetaum + thetadois*dt;                   % Velocidade angular
    
    theta = theta*180/pi;           % Trasnformacao de radianos para graus  
    
% ======================== NO AR ==========================================
    else

    % Forcas & Momentos (Pitch Rate - Q)
    Lq = 0.25*CLq*thetaum*flc.rho*Voo*Sref;
    Mq = 0.25*Cmq*thetaum*flc.rho*Voo*Cref*Sref;        
        
     % ==== Rotacionando ====
     for j = 1:ProfID-1 
        % Asas
        Lw(:,j) = [0; 0; 0.5*flc.rho*(Voo^2)*Sref*coeff_aoa.fit(j).CL(alpha)+Lq];
        Dw(:,j) = [0.5*flc.rho*(Voo^2)*Sref*coeff_aoa.fit(j).CD(alpha);0 ; 0];
        Mw(:,j) = [0; 0.5*flc.rho*(Voo^2)*Sref*coeff_aoa.fit(j).Cm25(alpha)*Cref + Mq; 0];
    
        M(:,j) = cross(CA_cg(:,j),(Etheta*(Lw(:,j)+Dw(:,j))));
     end 
     
        % Profundor
        Lt = [0; 0; 0.5*flc.rho*(Voo^2)*Sref*(coeff_aoa.fit(ProfID).CL(alpha)+CLprof)];
        Dt = [0.5*flc.rho*(Voo^2)*Sref*coeff_aoa.fit(ProfID).CD(alpha); 0; 0];
        Mt = [0; 0.5*flc.rho*(Voo^2)*Sref*coeff_aoa.fit(ProfID).Cm25(alpha)*Cref; 0];
    % =====================================
    
    Mprof = cross(CAt_cg,(Ealpha*(Lt+Dt)))+Mt;        

    Dparasita = 0.5*flc.rho*(Voo^2)*0.85*CDparasita;
    
    % 2 - Resultantes no motor
    T = [-empuxo(Voo,geo.mp.conjunto,flc.rho);0;0];
    Mmotor = cross(ENG_cg,T);
    
    % === Forcas verticais ===
    V = (sum(Lw(3,:))+Lt(3))*cosd(gamma)+T(1)*sind(theta)-(sum(Dw(1,:))+Dt(1)+Dparasita)*sind(gamma)-m*flc.g; % Soma das for�as verticais
    
    az = V/m;
    Sz = Sz + Vz*dt + 0.5*az*dt^2;
    Vz = Vz + az*dt;  
    
    % === Forcas horizontais ===
    ax = -(T(1)*cosd(theta)+(sum(Dw(1,:))+Dt(1)+Dparasita)*cosd(gamma)+(sum(Lw(3,:))+Lt(3))*sind(gamma)+N*flc.mi)/m;
    
    Sx = Sx + Vx*dt + 0.5*ax*dt^2;
    Vx = Vx + ax*dt;
        
    % === Momento total ===
    Mtotal = sum(M(2,:))+sum(Mw(2,:))+Mprof(2)+Mmotor(2);
    
    if  Mtotal < 0 && theta == 0 
        Mtotal = 0;
    end
    
%      if alpha >= std.arf-1 
%          thetaum = 0;
%          Mtotal = 0;
%      end    
    gamma = atand(Vz/Vx);
    
    theta = theta*pi/180;       % Trasnformacao de graus para radianos
    
    thetadois = Mtotal/Iyy_cg;     % Aceleracao angular
    theta = theta + thetaum*dt + thetadois*dt^2/2; 
    thetaum = thetaum + thetadois*dt; % Velocidade angular

    theta = theta*180/pi;       % Trasnformacao de radianos para graus 
    
         if Sz > 0.1*alt
            deltaealivio = 0.5*std.range;
         else
            deltaealivio = 0.3*std.range;
         end    
%     if Sx < Stot && Sz < alt && manter
%          if (alt-Sz)/Vz < (Stot-Sx)/Vx
%             deltaealivio = deltaealivio - 0.5*deltaeSpeed*dt;
%          else
%             deltaealivio = max(deltaealivio + deltaeSpeed*dt, 0);
%          end
%     elseif manter
%          if az > 0.5
%              deltaealivio = deltaealivio - 0.5*deltaeSpeed*dt;
%          else
%              deltaealivio = max(deltaealivio + deltaeSpeed*dt, 0);
%          end 
%     end
    
    
    if Sx >= Stot && Sz >= alt && ~passou
        fprintf('Passou com %4.1f kg\n',m);
        passou = 1;
    elseif Sx >= Stot && ~passou
        fprintf('Nao passou com %4.1f kg\n',m);
        break
    end
        
    if Sz <= 0 || Sx > 2*Stot
        fprintf('Caiu com %4.1f kg\n',m);
        passou = 0;
        break 
    elseif Sz >= 0.1*alt && ~manter
         manter = 1;
    elseif Sx > Stot+10 && Vz > 0
        fprintf('Continuou com %4.1f kg\n',m);
        break
    else
%         fprintf('Aconteceu algo que Vitor nao sabe com %4.1f kg\n',m);
%         break
    end
    
    end
    
    t = t + dt;
        
    % Salvando
    s_salvo(cont) = Sx;
    v_salvo(cont) = Vx;
    v2_salvo(cont) = Vz;
    a_salvo(cont) = ax;
    a2_salvo(cont) = az;
    h_salvo(cont) = Sz;
    alpha_salvo(cont)   = alpha;
    gamma_salvo(cont)   = gamma;
    theta_salvo(cont)   = theta;
    deltae_salvo(cont)   = deltae;
    thetaum_salvo(cont)   = thetaum;
    thetadois_salvo(cont)   = thetadois;
    Lw_salvo(cont) = Lw(3);
    Dw_salvo(cont) = Dw(1);
    Lt_salvo(cont) = Lt(3);
    Dt_salvo(cont) = Dt(1);
    
    
    cont = cont + 1;
end
    if ~passou
       break 
    end
end
% === Plot ===
figure(1);
subplot(4,1,1)
plot(s_salvo,h_salvo)
title('Altura')

subplot(4,1,2)
plot(s_salvo,alpha_salvo,s_salvo,theta_salvo,s_salvo,gamma_salvo,s_salvo,deltae_salvo)
legend('Alpha','Theta','Gamma','Deltae')

subplot(4,1,3)
plot(s_salvo,thetaum_salvo,s_salvo,thetadois_salvo)
legend('Thetaum','Thetadois')

subplot(4,1,4)
plot(s_salvo,Lt_salvo)
title('Sustenta��o profundor')

figure(2)
subplot(2,1,1)
plot(s_salvo,v_salvo,s_salvo,v2_salvo)
title('Velocidade')

subplot(2,1,2)
plot(s_salvo,a_salvo,s_salvo,a2_salvo)
title('Aceleracao')

figure(3)
subplot(2,1,1)
plot(s_salvo,Lw_salvo,s_salvo,Lt_salvo)
title('Sustenta��es')

subplot(2,1,2)
plot(s_salvo,Dw_salvo,s_salvo,Dt_salvo)
title('Arrastos')

% Fim
mtow = m-0.1;
