% FUN��O PARA DEFINIR PERFIL
% Desenvolvida para selecionar o perfil da asa
% Ordem crescente de Cl para aoa 5�
function [perfil] = seleciona_perfil(choice_perf)
    if  choice_perf == 0 
        perfil = 'WTU_01.mat';
    elseif choice_perf == 1 
        perfil = 'dae11b.mat';
    elseif choice_perf == 2 
        perfil = 'BA7_top10_WTU_01b.mat';
    elseif choice_perf == 3
        perfil = 'e591b.mat';
	elseif choice_perf == 4
		perfil = 'h2022_3b.mat';        
    elseif choice_perf == 5
        perfil = 'ETW_02_9_5b.mat';     

% Perfis - NACA 
    elseif choice_perf == 20
        perfil = 'prof2010.mat';
    elseif choice_perf == 21
        perfil = 'NACA_2412.mat';
    elseif choice_perf == 22
        perfil = 'NACA_3412.mat';
    elseif choice_perf == 23
        perfil = 'perfil_empH_neg_2016.mat'; 
    elseif choice_perf == 24
        perfil = 'WORTMANN_FX_63_137_EH.mat'; 
% === ANTIGO ===
%     if  choice_perf == -1 
%         perfil = 'WTU_01.mat';
%     elseif  choice_perf == 0 
%         perfil = 'BA7_top10_WTU_01b.mat';
%     elseif choice_perf == 1
%         perfil = 'CHP01_342_50b.mat';
%     elseif choice_perf == 2
%         perfil = 'CHP01_8b.mat';
% 
%     elseif choice_perf == 3
%         perfil = 'fx76mp120b.mat';
%     elseif choice_perf == 4
%         perfil = 'CHP01_9b.mat';
%     elseif choice_perf == 5
%         perfil = 'CHP01_9_5b.mat';
% 
%     elseif choice_perf == 6
%         perfil = 'h2022_1b.mat';
% 	elseif choice_perf == 7
% 		perfil = 'h2022_3b.mat';
%     elseif choice_perf == 8
% 		perfil = 'h2022_4b.mat';
%         
%     elseif choice_perf == 9
%         perfil = 'ch10_smoothed_b.mat';
%     elseif choice_perf == 10
%         perfil = 'ETW_02_9_5b.mat';
% 	elseif choice_perf == 11
% 		perfil = 'h2022_7b.mat';
%         
%     elseif choice_perf == 12
% 		perfil = 'ETW_02_10_5b.mat';      
%     elseif choice_perf == 13
% 		perfil = 'ETW_02b.mat';   
%     elseif choice_perf == 14
%         perfil = 'ETW_02_11_5b.mat';
%     elseif choice_perf == 15
%         perfil = 'CHP_01b.mat';
%     elseif choice_perf == 16
% 		perfil = 'ETW_02_12b.mat';
% 
% 
% % Perfis - NACA     
%     elseif choice_perf == 20       
%         perfil = 'NACA0012.mat';
%     elseif choice_perf == 21
%         perfil = 'NACA_1412.mat';
%     elseif choice_perf == 22
%         perfil = 'NACA_2412.mat';
%     elseif choice_perf == 23
%         perfil = 'NACA_3412.mat';
%     elseif choice_perf == 24
%         perfil = 'NACA_4412.mat';
%     elseif choice_perf == 25
%         perfil = 'x12_JOUKOWSKI.mat';
%     elseif choice_perf == 26
%         perfil = 'x2l_v3.mat';
%     elseif choice_perf == 27
%         perfil = 'PROF04.mat';
%     elseif choice_perf == 28
%         perfil = 'PROF01.mat';
%     elseif choice_perf == 29
%         perfil = 'prof2010.mat';
%     elseif choice_perf == 30
%         perfil = 'perfil_empH_neg_2016.mat'; 
%     elseif choice_perf == 31
%         perfil = 'WORTMANN_FX_19_137.mat'; 
%     elseif choice_perf == 32
%         perfil = 'WORTMANN_FX_63_137_EH.mat'; 
        % % Outros 
%     elseif choice_perf == 14
%         perfil = 'NACA_2412.mat';  
%     elseif choice_perf == 15
%         perfil = 'PROF04.mat';
%     elseif choice_perf == 16
%         perfil = 'x2l_v3.mat';
%     elseif choice_perf == 17
%         perfil = 'WORTMANN_FX_19_137.mat';
%     elseif choice_perf == 18
%         perfil = 'MSHD_2309i.mat';
%     elseif choice_perf == 19
%         perfil = 'WORTMANN_FX_63_137_EH.mat';
%     elseif choice_perf == 26
%         perfil = 'NACA0010.mat';
%     elseif choice_perf == 27
%         perfil = 'NACA0011.mat';
%     elseif choice_perf == 28
%         perfil = 'CHP01_342_50.mat';
%     elseif choice_perf == 29
%         perfil = 'EPPLER_342.mat';
    end
end