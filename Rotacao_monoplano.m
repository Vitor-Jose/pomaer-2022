%% Rotacao monoplano
% Marco de 2022
clear; close all; clc;

load('nova85eh');          % Carrega geometria
load('ex_flc');             % Carrega condicoes de voo

% Redefinicoes
% geo.s.pos(2,1) = -1.3;
geo.s.b(2,2) = 0.3;
% geo.tp.pos(1) = -0.01;
% geo.s.perfil(2,1:2) = {'ETW_02b' 'ETW_02b'};
geo.s.perfil(2,:) = {'NACA_2412' 'NACA_2412' []};
% save('geo95nova','geo'); 
geo.arf = 19;               % Angulo de arfagem

flc.Voo = 11.8;             % Velocidade de Voo

std.m   = 13.9;             % Massa
std.Iyy = 1;                % Momento de inercia
std.a   = 0.1;              % Aceleracao

deflexao = 0;
geo.s.ai(2) = -12;

ProfID  = geo.surfacenum;   % Identidade do profundor
% Inicializacao 
i       = 1;
t       = 0;
theta   = 0;        % Angulo entre referenciais
dt      = 0.001;    % Infinitesimal de tempo
thetaum = 0;        % Vel. angular

%% Posicoes (Conferir referencial no exemplo)
CA = zeros(3,geo.surfacenum);                % Prealocacao
for aux_s = 1:geo.surfacenum
    CA(:,aux_s) = [geo.s.pos(aux_s,1) - geo.tp.pos(1);0;geo.s.pos(aux_s,3) - geo.tp.pos(3)];
end 
CAt = CA(:,ProfID);

% Centro de gravidade
Xcg = -geo.tp.pos(1);
Zcg = -geo.tp.pos(3);
CG  = [Xcg;0;Zcg];

% Motor
Zmotor = geo.motor.pos(3)-geo.tp.pos(3);    % Conferir soma de tp
thrust = @(v) (0.0004.*v.^3 - 0.0055*v.^2 - 1.2483*v + 46.7484);
T      = [thrust(flc.Voo); 0 ;0];
ENG    = [0;0;Zmotor];

% Peso
W = [0;0;std.m*flc.g];

% Forca inercial
Finercia = [-std.a*std.m;0;0];

%% VLM
flc.case(1).aoa = [0:2:14 15:geo.arf];
% flc.case(1).aoa = [0:5:15];
coef = coeffs_22(geo,flc,2,1:ProfID,1);

% flc.case(2).aoa = -14:2:4;
% coef = coeffs_22(geo,flc,1,[1:ProfID-1;ProfID 0*(1:ProfID-2)],1);
% load('coeficientes.mat')
% save('coeficientes.mat','coef')

for j=1:geo.surfacenum
    coeff.fit(j).CL = fit(coef(j).aoa',coef(j).CL','linearinterp');
    coeff.fit(j).CD = fit(coef(j).aoa',coef(j).CD','linearinterp');
    coeff.fit(j).Cm25 = fit(coef(j).aoa',coef(j).Cm25','linearinterp');
end

%% CL delta e
flc.case(1).aoa = [0 -6];
coeffsEH = coeffs_22(geo,flc,0,ProfID,ProfID);
CL_deltae = (coeffsEH(ProfID).CL(1) - coeffsEH(ProfID).CL(2))/(coeffsEH(ProfID).aoa(1) - coeffsEH(ProfID).aoa(2))*geo.s.neta(ProfID)*coeffsEH(ProfID).Sref/coef(1).Sref;
CD_deltae = (coeffsEH(ProfID).CD(1) - coeffsEH(ProfID).CD(2))/(coeffsEH(ProfID).aoa(1) - coeffsEH(ProfID).aoa(2))*geo.s.neta(ProfID)*coeffsEH(ProfID).Sref/coef(1).Sref;

%% LOOP 
while theta < geo.arf
    E1 = [cosd(theta) 0 -sind(theta); 0 1 0 ; sind(theta) 0 cosd(theta)];
    
    % Momento das asas
    for j = 1:ProfID-1
        alpha = theta - geo.s.deda(j)*theta - geo.s.eo(j); % ?
        
        Lw(:,j) = [0;0;-geo.s.neta(j)*0.5*flc.rho*(flc.Voo^2)*coef(1).Sref*coeff.fit(j).CL(alpha)];
        Dw(:,j) = [-geo.s.neta(j)*0.5*flc.rho*(flc.Voo^2)*coef(1).Sref*coeff.fit(j).CD(alpha);0;0];
        Mw(:,j) = [0; 0.5*geo.s.neta(j)*flc.rho*(flc.Voo^2)*coef(1).Sref*coeff.fit(j).Cm25(alpha)...
        *coef(1).Cref;0];
    
        M(:,j) = cross(CA(:,j),(E1*(Lw(:,j)+Dw(:,j))));
    end
    
    % Momento do profundor
    Lt = [0;0;-0.5*geo.s.neta(ProfID)*flc.rho*(flc.Voo^2)*coef(1).Sref*coeff.fit(ProfID).CL(alpha)-CL_deltae*deflexao];
    Dt = [-0.5*geo.s.neta(ProfID)*flc.rho*(flc.Voo^2)*coef(1).Sref*coeff.fit(ProfID).CD(alpha)-CD_deltae*deflexao;0;0];
    Mt = [0; 0.5*flc.rho*(flc.Voo^2)*coef(1).Sref*coeff.fit(ProfID).Cm25(alpha)...
        *coef(1).Cref*geo.s.neta(ProfID);0];
    
    Mprof = cross(CAt,(E1*(Lt+Dt)))+Mt;
    
    % Momento do motor
    Mmotor = cross(ENG,T);
    
    % Momento inercial
    Mcg = cross(CG,(E1*(Finercia+W)));
    
    % Momento totalLt
    Mtotal = sum(M(2,:))+sum(Mw(2,:))+Mprof(2)+Mmotor(2)+Mcg(2);
    
    if  Mtotal < 0 && theta == 0
        Mtotal = 0;
    end
    
    theta = theta*pi/180;       % Trasnformacao de graus para radianos
    thetadois = Mtotal/std.Iyy; % Aceleracao angular

    thetaum = thetaum + thetadois*dt; % Velocidade angular
    theta = theta + thetaum*dt + thetadois*dt^2/2; 
    
    t = t + dt;
    theta = theta*180/pi; % Trasnformacao de radianos para graus
    
    % Para o plot
    Mtotal_plot(i) = Mtotal; 
    theta_plot(i) = theta;
    tempo_plot(i) = t;
    % Mcg = cross(CG,(E1*W))+cross(CG,(E1*Finercia));
    Mcg_plot(i) = Mcg(2);
    M_prof_plot(i) = Mprof(2);
    M_motor_plot(i) = Mmotor(2);
    
for j = 1:ProfID-1
    M_plot(i,j) = M(2,j)+Mw(2,j);
end
    i = i+1;
    
    if t > 2
        theta = geo.arf + 2;
        disp('Aeronave n�o rotaciona.');
    end 
end

figure(1); hold on
for j = 1:ProfID-1
    plot(tempo_plot,M_plot(:,j))
    legenda(j) = {['Asa ' num2str(j)]};
end
plot(tempo_plot,Mcg_plot,tempo_plot,M_prof_plot,tempo_plot,M_motor_plot,tempo_plot,Mtotal_plot);
legenda = {legenda{1:end} 'For�as Inerciais' 'EH + Profundor' 'Motor' 'Momento Total'};
legend(legenda)
xlabel('Tempo (s)')
ylabel('Momento (N*m)')
grid on

figure(2)
plot(tempo_plot, theta_plot)
xlabel ('Tempo (s)')
ylabel ('AoA (graus)')
grid on

figure(3)
aoa = 0:.5:geo.arf;
plot(aoa,coeff.fit(1).CL(aoa)+coeff.fit(2).CL(aoa))
ylabel ('CL global')
xlabel ('AoA (graus)')
grid on
