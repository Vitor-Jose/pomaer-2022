function [result] = modelo(x,~,individuo)

% MODELO DE OTIMIZACAO DA AERONAVE MONOPLANO 2020
% TUCANO 2020 - TORNEIO DE ACESSO

%% -------------------------- INICIALIZACAO -------------------------------
% PARAMETROS DA SIMULACAO
tic;                % inicio da contagem de tempo
plotar = 0;         % 0: nao plota
                    % 1: plota a malha VLM
                    % 2: plota a geometria com perfis utilizados

sim.panel    = 5;   % [-] numero parametrizado de paineis VLM
sim.intermax = 20;  % [-] numero de iteracoes maxima no VLM

% PARAMETROS GEOMETRICOS DA AERONAVE (VARIAVEIS DE OTIMIZACAO)
% 01 [ ] Alongamento das asas
% 02 [m] Envergadura das asas
% 03 [ ] Tamanho percentual da estacao central + media em relacao a envergadura (ambas asas)
% 04 [ ] Tamanho percentual da estacao central em relacao a estacao central + media (ambas asas)
% 05 [ ] Afilamento da estacao media (ambas asas)
% 06 [ ] Afilamento da estacao ponta (ambas asas)
% 07 [deg] angulo de incidencia asa inferior
% 08 [deg] angulo de incidencia asa superior
% 09 [ ] Selecao do perfil asa inferior
% 10 [ ] Selecao do perfil asa superior 
% 11 [m] Stagger entre asas (positivo pra tras)
% 12 [m] Gap entre asas (positivo pra cima)
% EH
% 13 [m] Envergadura da empenagem horizontal
% 14 [m] Corda da empenagem horizonal
% 15 [deg] Incidencia da empenagem horizontal
% 16 [ ] Efetividade do profundor (tau)
% CG 
% 17 [ ] Posicao longitudinal do CG em funcao da corda da raiz

aeronave.asas    = [2.39 x(1) x(2) x(3) x(4) x(5) floor(x(6)) 0];
% ['Envergadura da asa' 'Alongamento da asa' 'Razao da secao 2' 'Razao da secao 3' 'Afilamento 2' 'Afilamento 3' 'Perfil asa' 'Incidencia Asa'];
aeronave.eh      = [x(7) x(8) x(10) 0 23 1 x(9)];
% ['Envergadura da eh' 'Corda da eh' 'Altura em rela��o a asa' 'Incidencia da EH' 'Perfil EH' 'Efetividade do profundor' 'LE'];
aeronave.ev      = [0.25 0.00 23];
% ['Envergadura da ev' 'Posicao do centro em relacao ao CA da EH' 'Perfil EV'];
aeronave.outros  = [x(11) 0.02 0];
% ['Posicao do CG em funcao da corda' 'Posicao do tp em rela��o ao CG' 'Altura do motor'];

% GEOMETRICO

[geo,penal] = geometrico(aeronave);

% CONSTANTES INICIAIS
[flc,~] = condicoesVOO();

% DISPLAY
[~] = VLManda(geo,flc,sim,plotar,'-LiftingSurfaces');


%% ------------------------- AERODINAMICA ---------------------------------
if ~penal
% Calculo da area molhada das empenagens
[geo.ev.Swet]   	= areaMolhada_empenagem(geo.ev.c(1),geo.ev.c(2),geo.ev.B,'NACA0012');

else 
    ard.alpha_estol = 0;
    ard.CL_max = 0;
    disp('Aeronave penalizada no geom�trico');
end

%% -------------------- ESTABILIDADE E CONTROLE ---------------------------
if ~penal 
    % Funcao para calculo do alpha_estol, deltae, areas das superficies e
    % MAC (Dentro da estabilidade)
    [penal,ard,geo.LiftingSurface.MAC]         = estabilidade(geo,flc,sim);
end    
    
%% -------------------------- PONTUACAO -----------------------------------
if penal
	% Estimativa de mtow para aeronaves penalizadas
	mtow   			= 5;

	% Estimativa de PV para aeronaves penalizadas
	pv 				= 2.8;

	cp 				= mtow - pv; % carga paga maxima da aeronave

else
	% Calculo do MTOW da aeronave pelo modulo de dsp
	[mtow]   		= corrida(geo, flc, sim, ard);
    if mtow > 10.9
        % Calculo do PV da aeronave pelo estima massa
        [pv] 		= EstimaMassa(geo,mtow); %%%% so rodar com o femap instalado, caso contr�rio comentar e usar pv fixo
        fclose('all');
        %pv = 3;
    else
        pv = 2.8;
    end
	cp = mtow - pv; % carga paga maxima da aeronave
 end

[Pontuacao] 		= pontuacao(cp, pv);
result              = -Pontuacao;
tempo = toc;

%% -------------------------------- DISPLAY --------------------------------
fprintf('Aeronave n: %d; Iter: %d; Ind: %d\n', individuo(3), individuo(1), individuo(2))
disp('');
fprintf('MTOW = %f\n', mtow);
fprintf('PV = %f\n', pv);
fprintf('PONTOS = %f\n', Pontuacao);
disp('');
fprintf('Envergadura = %.3f\n', geo.LiftingSurface.B(1));
fprintf('Comprimento = %.3f\n', geo.comprimento);
fprintf('le = %f\n', geo.LiftingSurface.le);
disp('');
disp('Vetor x: '); disp(mat2str(x));
fprintf('TEMPO = %.3f\n',tempo);
fprintf('ALPHA ESTOL = %.3f\n',ard.alpha_estol);
fprintf('CL Maximo= %.3f\n',ard.CL_max);
disp('===================================================================');
end
