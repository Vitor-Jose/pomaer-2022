% function cmalpha(geo,flc)
clear; close all; clc 
%% ==================== Prealocacaoo e Inicialização =======================
load('nova85eh');
load('ex_flc');

% geo.o.panel         = 10;                                                    % Numero de paineis por secao de LS (Ideal: 25)
% geo.o.itermax       = 10;
%geo.s.b(2,2)        = 0.45;
i = 1;
aoamin = -5;
aoamax = 18;
inc = 0.1;
% for j = 1:geo.surfacenum                                                                                           % Posição dos Centros Aerodinâmicos
%     Xca(j) = geo.s.pos(j,1); % - geo.tp.pos(1);
% end 

%% ========================= VEM COM NOIS VLM =============================
flc.Voo = 11.8; 
flc.case(1).aoa = (aoamin:2:aoamax); 
coef = coeffs_22(geo,flc,2,[1 2],1);                                         % Vem pro pai, VLM
for j=1:geo.surfacenum
    coeff.fit(j).CL = fit(coef(j).aoa',coef(j).CL','linearinterp');         % Nesse loop e feito o fit dos coeficientes (apenas para simplificar o acesso aos coeficientes)
    coeff.fit(j).CD = fit(coef(j).aoa',coef(j).CD','linearinterp');
    coeff.fit(j).Cm25 = fit(coef(j).aoa',coef(j).Cm25','linearinterp');
end

%% ========================= Cálculo do Cm ================================
for aoa = aoamin:inc:aoamax
    for j=1:geo.surfacenum
    M = geo.s.neta(j)*[0 -geo.s.pos(j,3) geo.s.pos(j,2); geo.s.pos(j,3) 0 -geo.s.pos(j,1); -geo.s.pos(j,2) geo.s.pos(j,1) 0]*([cosd(aoa) 0 -sind(aoa);0 1 0;sind(aoa) 0 cosd(aoa)]*[-coeff.fit(j).CD(aoa-geo.s.deda(j)*aoa-geo.s.eo(j));0;-coeff.fit(j).CL(aoa-geo.s.deda(j)*aoa-geo.s.eo(j))]);
    Cm(j) = M(2)/coef(1).Cref + geo.s.neta(j)*coeff.fit(j).Cm25(aoa - geo.s.deda(j)*aoa - geo.s.eo(j));
    end
    Cm_aeronave = sum(Cm);
    Cm_plot(i) = Cm_aeronave;
    alpha_plot(i) = aoa;
    i = i+1;
end    


% openfig('cm_vsp');
% hold on
plot(alpha_plot,Cm_plot)
grid on
xlabel('Alpha (Graus)')
ylabel('Cm Total')
title('Cm x alpha')
hold on




        





