function [pv,M] = EstimaMassa (geo, mtow)

cd 'Estimativa_de_massa'

%% DADOS DE ENTRADA %%
env.asa   = geo.LiftingSurface.B(1,1);                                 % [m] Envergadura da asa
cr.asa    = geo.LiftingSurface.c(1,1);                                 % [m] Corda da raiz da asa
cp.asa    = geo.LiftingSurface.c(1,3); 		                           % [m] Corda da ponta da asa
ret.asa   = geo.LiftingSurface.pest(1,1);                              % Porcentagem retangular da asa
%gap       = geo.LiftingSurface.Gap;
%
MTOW      = mtow;					                                   % [kg] MTOW
% 
env.prof  = geo.LiftingSurface.B(2,1);					               % [m] Envergadura profundor
cr.prof   = geo.LiftingSurface.c(2,1);				                   % [m] Corda da raiz profundor
cp.prof   = geo.LiftingSurface.c(2,2);				                   % [m] Corda da ponta profundor
% 
env.leme = geo.ev.B;					                               % [m] Envergadura leme
cr.leme  = geo.ev.c(1);				                                   % [m] Corda da raiz leme
cp.leme  = geo.ev.c(2); 				                               % [m] Corda da ponta leme
%
comp.boom = geo.LiftingSurface.le - 0.75*geo.LiftingSurface.c(1,1);    % [m] Comprimento do boom no eixo x
h.boom    = geo.LiftingSurface.pos(2,3);                               % [m] Altura do boom no eixo z
lst.boom  = geo.tail.ecc;                                              % [m] Largura da se��o transversal do boom 
hst.boom  = geo.tail.hcc;                                              % [m] Altura da se��o transversal do boom 
%
% choice	 = geo.mp.conjunto;

%% MASSAS %%

M.Rodab  = 0.028;  % Roda bequilha sem oring 0.02
M.Rodat  = 0.030; % Roda trem de pouso sem oring 0.025
M.Oring  = 0.006; % Oring
M.Mancal = 0.012; % Mancal da bequilha
M.Rola   = 0.004; % Rolamento
M.Trem   = 0.050; % Trem de pouso
M.Par    = 0.006; % Parafusos, arruelas e porcas
M.Tanque = 0.022; % Tanque de combust�vel
M.Beq    = 0.012; % Bequilha
M.Fus    = 0.160; % Fuselagem entelada
M.Fio    = 0.013; % [kg/k] Fia��o, com conector, por metro
M.Servop = 0.045;  % Servo profundor 0.03
M.Servol = 0.017*4; % Servo leme
M.Servob = 0.018; % Servo bequilha
M.Servom = 0.01;  % Servo motor
M.Ubeck  = 0.028; % Ubeck
M.Bat    = 0.042; % Bateria
M.Recep  = 0.023; % Receptor
M.Freio  = 0.035; % Conjunto freio com servo
M.Volt   = 0.005; % Voltwatch
M.Mot    = 0.048 + 0.520; % Motor Eletrico : esperando pesar, no momento chutando choice 3

% % % Motor El�trico 
% if choice == 0
%     M.Mot = .048 + .688; % H�lice 1 + Motor 1
% elseif choice == 1
%     M.Mot = .040 + .688; % H�lice 2 + Motor 1
% elseif choice == 2
%     M.Mot = .040 + .520; % H�lice 3 + Motor 1
% elseif choice == 3
%     M.Mot = .048 + .520; % H�lice 4 + Motor 1
% elseif choice == 4
%     M.Mot = .048 + .520; % H�lice 5 + Motor 1
% end
%%
% if geo.w.flap > 0
%      M.Flap = 0.11;
% else
       M.Flap = 0;
% end
% % 
% if geo.w.e(1) >= 5 && geo.w.e(2) >= 10
%      M.Enf1 = 0.05;
% else
       M.Enf1 = 0;
% end
% %     
% if geo.w.pb(2) >= 0.03 && geo.w.e(2) >= 10
%     M.Enf2 = 0.05;
% else
       M.Enf2 = 0;
% end
% % 
% if geo.w.d(2) < -3
%      M.died = 0.02;
% else
       M.died = 0;
% end


%%
[M.Asa,Rtubo] = Asa_balsa (env.asa,cr.asa,cp.asa,ret.asa,MTOW);              % Massa asa completa

[M.Prof]      = Profundor (env.prof,cr.prof,cp.prof);                         % Massa profundor completo
[M.Leme]      = Leme (env.leme,cr.leme,cp.leme);                              % Massa leme
[M.Boom]      = Tailboom (comp.boom,h.boom,lst.boom,hst.boom);                % Massa boom                                                    % Massa Montantes da aeronave

M.Fuscomp     = M.Fus+M.Mancal+4*M.Par+M.Tanque+M.Servob+M.Servom;            % Fuselagem completa
M.Conjtrem    = M.Rodab+2*M.Rodat+3*(M.Oring+M.Rola)+M.Trem+M.Beq+M.Freio;    % Conjunto trem de pouso
M.Emp         = M.Servop+M.Servol;              				    		  % Empenagens
M.Elet        = 2*M.Fio+M.Ubeck+M.Bat+M.Recep+M.Volt;                         % El�trica

pv = M.Asa+M.Fuscomp+M.Conjtrem+M.Elet+M.Mot+M.Flap+M.Enf1+M.Enf2+M.died+M.Prof+M.Emp+M.Leme+M.Boom;    % Peso vazio total

M.PesoVazio = pv;

cd ..
end