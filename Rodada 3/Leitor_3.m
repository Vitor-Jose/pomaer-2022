clear;clc;close all; 
%% INICIALIZACAO
arquivos = {'Otimizacao_thales3' 'Otimizacao_thales3_2' 'Otimizacao_josep2'};
for tt = 1:length(arquivos)
clearvars -except arquivos tt
fprintf('\n %s \n',arquivos{tt});
load(arquivos{tt})             % Importando o arquivo de melhores aeronaves

filtrar = 0;                   % Limpar aeronaves que nao passaram em STD
                               % 0 - Nao limpar 
                               % 1 - Limpar

plotar_limites = 1;            % Plotar limites
                               % 0 - Nao plota limites OB
                               % 1 - Plota limites OB
                               
plotar  =2;                   % Como plotar
                               % 0 - Nao plotar
                               % 1 - Plota cada vari�vel em figuras separadas
                               % 2 - Plota as variaveis na mesma figura
                               % 3 - Plota evolu��o das best
                               

%% CALCULO

I = ind_itermax;               % Trazendo as matrizes de valores 
otm = Individuos_otimizados;   % Individuo otimo
n = length(I);                 % N�mero de linhas da matriz
m = length(I(1,:));            % N�mero de colunas na matriz

if filtrar
    pos = find(I(:,1)<-122); 
else
    pos = find(I(:,1)<0);
end

Ipts  = I(pos,1);
Inovo = I(pos,2:m);

ptm = mean(Ipts);
ptm = - ptm;

media = mean(Inovo);
dp = std(Inovo);
cima = media + 2*dp;
baixo= media - 2*dp;

%% RETORNO
titulo = {'Alongamento Asa'
'Tamanho estacao central'
'Afilamento segunda secao'
'Perfil da estacao central'
'Perfil da ponta'
'Incidencia'
'Envergadura EH'
'Corda EH'    
'Perfil EH'
'LE EH'
'Velocidade de decolagem'         
};
fprintf('    VARIAVEL      MEDIA  DESVIO PADRAO INFERIOR  SUPERIOR      BEST\n');
for i = 1:length(titulo)
    if media(i) < 0
        fprintf('%25s:  %5.3f      %6.4f    %5.3f    %5.3f        %5.3f\n', titulo{i},media(i),dp(i),baixo(i),cima(i),otm(i));
    else
        fprintf('%25s:   %5.3f      %6.4f     %5.3f     %5.3f         %5.3f\n', titulo{i},media(i),dp(i),baixo(i),cima(i),otm(i));
    end
end

%% Plotar 
if plotar == 1
    for k = m-1:-1:1
        figure(k)
        var(k) = k+0.2*tt;
        plot(var(k),otm(k),'xr',var(k),media(k),'*b',var(k),cima(k),'*g',var(k),baixo(k),'*g')
        if plotar_limites && tt == 1
            hold on
            plot([var(k)-1 var(k)+1],[OB(k,1) OB(k,1)],'LineWidth',5)
            plot([var(k)-1 var(k)+1],[OB(k,2) OB(k,2)],'LineWidth',5)
        end
        grid on; grid minor;
        title(titulo{k})
        legend('Best','Media','Desvios')
    end
elseif plotar == 2
    figure(20)
    for k = m-1:-1:1
        subplot(3,4,k);
        var(k) = k+0.2*tt;
        plot(var(k),otm(k),'xr',var(k),media(k),'*b',var(k),cima(k),'*g',var(k),baixo(k),'*g')
        if plotar_limites && tt == 1
            hold on
            plot([var(k)-1 var(k)+1],[OB(k,1) OB(k,1)],'LineWidth',2)
            plot([var(k)-1 var(k)+1],[OB(k,2) OB(k,2)],'LineWidth',2)
        end
        grid on; grid minor;
        title(titulo{k})
    end
elseif plotar == 3
    figure(30)
    plot(1:length(Individuos_best),Individuos_best)
    hold on; grid on; grid minor;
    legend(arquivos)
end

%% Outros
% if plotar == 3
% var = categorical({'ARw', 'pb_1', 'pb_2', '\lambda_{2}', '\lambda_{3}', 'ai','perfil','B_{eh}', 'c_{eh}', 'LE', 'altura', 'CG'});
% var = reordercats(var,{'ARw', 'pb_1', 'pb_2', '\lambda_{2}', '\lambda_{3}', 'ai','perfil','B_{eh}', 'c_{eh}', 'LE', 'altura', 'CG'});
% 
% result = ind_itermax;
% 
% figure(2)
% subplot(2,1,1)
% bar(var,media)
% title('Media')
% grid on; grid minor;
% subplot(2,1,2)
% bar(var,dp)
% title('Desvio padrao')
% grid on; grid minor;
% end
 
% %% AERONAVES N�O PENALIZADAS NO ULTIMO CICLO
% j=1;
% for i=1:find(ind_itermax(:,1) > -120)-1
%     if ind_itermax(i,1) < -130
%         result2(j,:) = ind_itermax(i,2:18);
%         j = j+1;
%     end
% end
% 
% for i = 1:length(result2(1,:))
%     media2(i) = mean(result2(:,i));
%     desvio2(i) = std(result2(:,i));
%     desvio_m2(i) = desvio2(i)/abs(media2(i));
% end
% 
% figure(3)
% subplot(2,1,1)
% bar(var,media2)
% title('Media')
% subplot(2,1,2)
% bar(var,desvio2)
% title('Desvio padrao')
% 

 %% CONVERGENCIA DA PONTUACAO
% itr         = 1:itermax;
% Pont_itr    = -Individuos_best;
% Pont_itr    = fit(itr',Pont_itr','cubicinterp');
% Pont_itr    = Pont_itr(itr);
% 
% figure(1); grid on; grid minor;
% plot(itr, Pont_itr)
% xlabel('Iteracoes')
% ylabel('Pontuacao de voo')
% title('Convergencia da pontuacao por iteracoes')

end