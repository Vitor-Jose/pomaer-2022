% Codigo para testar os melhores resultados
clear; close all; clc;

arquivos = {'Otimizacao_LucaII'};
rodada = 'Rodada 4';

n_last = 1;

for tt = 1:length(arquivos)
    load(arquivos{tt})             % Importando o arquivo de melhores aeronaves
    
    % === Best ===
    otm = Individuos_otimizados;

%     otm(9) = 25;
%     otm(7) = 0.6;
%     [~,pos] = sort(ind_itermax(:,1));
%     disp([pos ind_itermax(pos,1)])
%     otm = ind_itermax(pos(n_last),2:end);   
%     otm(7) = 0.685;
    

    cd ..
    [result] = modelo(otm,0,[1 1 1]);
    load('Aeronave.mat');
    VLManda(geo,flc,sim,2,'-LiftingSurfaces');% So colar na command Window
    cd(rodada)
    
    save([arquivos{tt} '_plane'])
    
    figure(1)
    subplot(1,2,1)
    plot(0:24,stab.coeff_aoa.fit(1).CL(0:24))
    subplot(1,2,2)
    plot(0:24,stab.coeff_aoa.fit(2).CL(0:24))
    title('C_L')
    hold on
    
    figure(2)
    subplot(1,2,1)
    plot(0:20,stab.coeff_aoa.fit(1).CD(0:20))
    subplot(1,2,2)
    plot(0:20,stab.coeff_aoa.fit(2).CD(0:20))
    title('C_D')
    hold on
    
    figure(3)
    subplot(1,2,1)
    plot(0:20,stab.coeff_aoa.fit(1).Cm25(0:20))
    subplot(1,2,2)
    plot(0:20,stab.coeff_aoa.fit(2).Cm25(0:20))
    title('C_m')
    hold on
    
    % === Mean ===
%     I = ind_itermax;               % Trazendo as matrizes de valores 
%     media = mean(I(:,2:end));
%     
%     cd ..
%     [result] = modelo(media,0,[1 1 1]);
%     load('Aeronave.mat');
%     VLManda(geo,flc,sim,2,'-LiftingSurfaces');% So colar na command Window
%     cd(rodada)
%     
%     save([arquivos{tt} '_plane2'])
%     
%     figure(1)
%     subplot(1,2,1)
%     plot(0:20,stab.coeff_aoa.fit(1).CL(0:20))
%     subplot(1,2,2)
%     plot(0:20,stab.coeff_aoa.fit(2).CL(0:20))
%     title('C_L')
%     
%     figure(2)
%     subplot(1,2,1)
%     plot(0:20,stab.coeff_aoa.fit(1).CD(0:20))
%     subplot(1,2,2)
%     plot(0:20,stab.coeff_aoa.fit(2).CD(0:20))
%     title('C_D')
%     
%     figure(3)
%     subplot(1,2,1)
%     plot(0:20,stab.coeff_aoa.fit(1).Cm25(0:20))
%     subplot(1,2,2)
%     plot(0:20,stab.coeff_aoa.fit(2).Cm25(0:20))
%     title('C_m')
end
