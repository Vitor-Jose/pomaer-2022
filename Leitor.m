 clear; clc;              %Limpeza das vari�veis 
 
 %% INICIALIZA�AO
 load('Otimizacao3_2.mat')   %Importando o arquivo de melhores aeronaves
 plotar = 1;               %Plotar a compara��o com a melhor aeronave
 % x = [ 7.5639    0.8273    0.7000    0.6800    0.6290    0.8944    1.8245    0.8582    0.2119    0.9016   0.5331    0.2759];
 % x = [ 6.5000    0.8299    0.7000    0.7211    0.6247    0.1518   4.6363    0.9211    0.2003    0.7801    0.4545    0.2501];
 % x = [ 6.5000    0.8226    0.6980    0.7409    0.6216    0.9415    4.9900   0.9245    0.1987    0.7702    0.4776    0.2500];
 %% CALCULO
 I = ind_itermax;         %Trazendo as matrizes de valores 
 otm = Individuos_otimizados; 
 n = length(I);           %N�mero de linhas da matriz
 m = length(I(1,:));      %N�mero de colunas na matriz
 
 for x = 1:n
     if I(x,1) == 0
         n = x-1;         %Localizando qual o numero real linhas sem zeros prealocados
         break
     end
 end
 
 ptm = mean(I(:,1));
 ptm = - ptm;
 I = I(1:n,2:m);          %Retirada da pontua��o 
 media = mean(I);
 dp = std(I);
 cima = media + 2*dp;
 baixo= media - 2*dp;
 
 %% RETORNO
 disp('===================================================================');
 fprintf('Media da OTM : ');
 disp(media);
 fprintf('Desvio padrao: ');
 disp(dp);
 fprintf('XVmin = [');
 disp(baixo);disp(']');
 fprintf('XVmax = [');
 disp(cima);disp(']');
 disp('===================================================================');

 %% Plotar 
 if plotar == 1
     var = (1:m-1);
     otm = Individuos_otimizados; 
     
     subplot(3,4,[1 5]);
     k = 1;
     plot(var(k),otm(k),'xr',var(k),media(k),'*b',var(k),cima(k),'*g',var(k),baixo(k),'*g')
     title('Alongamento')
     grid on; grid minor;
     
     subplot(3,4,2)
     k = [2 3];
     plot(var(k),otm(k),'xr',var(k),media(k),'*b',var(k),cima(k),'*g',var(k),baixo(k),'*g')
     xlim([1.5 3.5])
     title('Tamanhos das se��es')
     grid on; grid minor;
     
     subplot(3,4,3)
     k = [4 5];
     plot(var(k),otm(k),'xr',var(k),media(k),'*b',var(k),cima(k),'*g',var(k),baixo(k),'*g')
     xlim([3.5 5.5])
     title('Afilamentos')
     grid on; grid minor;
     
     subplot(3,4,6)
     k = 6;
     plot(var(k),otm(k),'xr',var(k),media(k),'*b',var(k),cima(k),'*g',var(k),baixo(k),'*g')
     title('Incid�ncia')
     grid on; grid minor;
     
     subplot(3,4,4)
     k = 7;
     plot(var(k),otm(k),'xr',var(k),media(k),'*b',var(k),cima(k),'*g',var(k),baixo(k),'*g')
     title('Perfil')
     grid on; grid minor;
     
     subplot(3,4,7)
     k = 8;
     plot(var(k),otm(k),'xr',var(k),media(k),'*b',var(k),cima(k),'*g',var(k),baixo(k),'*g')
     title('Envergadura EH')
     grid on; grid minor;
     
     subplot(3,4,8)
     k = 9;
     plot(var(k),otm(k),'xr',var(k),media(k),'*b',var(k),cima(k),'*g',var(k),baixo(k),'*g')
     title('Corda EH')
     grid on; grid minor;
     
     subplot(3,4,9)
     k = 10;
     plot(var(k),otm(k),'xr',var(k),media(k),'*b',var(k),cima(k),'*g',var(k),baixo(k),'*g')
     title('LE')
     grid on; grid minor;
     
     subplot(3,4,10)
     k = 11;
     plot(var(k),otm(k),'xr',var(k),media(k),'*b',var(k),cima(k),'*g',var(k),baixo(k),'*g')
     title('Altura EH')
     grid on; grid minor;
     
     subplot(3,4,11)
     k = m - 1;
     plot(var(k),otm(k),'xr',var(k),media(k),'*b',var(k),cima(k),'*g',var(k),baixo(k),'*g')
     title('CG')
     grid on; grid minor;  
     
     legend('Best','Media','Desvios')
 end
 
if plotar == 3
var = categorical({'ARw', 'pb_1', 'pb_2', '\lambda_{2}', '\lambda_{3}', 'ai','perfil','B_{eh}', 'c_{eh}', 'LE', 'altura', 'CG'});
var = reordercats(var,{'ARw', 'pb_1', 'pb_2', '\lambda_{2}', '\lambda_{3}', 'ai','perfil','B_{eh}', 'c_{eh}', 'LE', 'altura', 'CG'});

result = ind_itermax;

figure(2)
subplot(2,1,1)
bar(var,media)
title('Media')
grid on; grid minor;
subplot(2,1,2)
bar(var,dp)
title('Desvio padrao')
grid on; grid minor;
 end
% %% AERONAVES N�O PENALIZADAS NO ULTIMO CICLO
% j=1;
% for i=1:find(ind_itermax(:,1) > -120)-1
%     if ind_itermax(i,1) < -130
%         result2(j,:) = ind_itermax(i,2:18);
%         j = j+1;
%     end
% end
% 
% for i = 1:length(result2(1,:))
%     media2(i) = mean(result2(:,i));
%     desvio2(i) = std(result2(:,i));
%     desvio_m2(i) = desvio2(i)/abs(media2(i));
% end
% 
% figure(3)
% subplot(2,1,1)
% bar(var,media2)
% title('Media')
% subplot(2,1,2)
% bar(var,desvio2)
% title('Desvio padrao')
% 

 %% CONVERGENCIA DA PONTUACAO
% itr         = 1:itermax;
% Pont_itr    = -Individuos_best;
% Pont_itr    = fit(itr',Pont_itr','cubicinterp');
% Pont_itr    = Pont_itr(itr);
% 
% figure(1); grid on; grid minor;
% plot(itr, Pont_itr)
% xlabel('Iteracoes')
% ylabel('Pontuacao de voo')
% title('Convergencia da pontuacao por iteracoes')
 