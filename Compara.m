clear; clc;

%% Colocar as matrizes 
media(1,:) = [     7.0014    0.8268    0.6917    0.7211    0.6281    0.4061    3.3226    0.8870    0.1976    0.9162    0.4375    0.2649];
dp(1,:)    = [     0.5872    0.0183    0.0212    0.0271    0.0108    0.3910    1.6617    0.0851    0.0168    0.1647    0.1669    0.0176];
media(2,:) = [     6.5030    0.8251    0.6994    0.7313    0.6229    0.3071    4.6517    0.9226    0.2011    0.7697    0.4934    0.2503];
dp(2,:)    = [     0.0042    0.0068    0.0007    0.0115    0.0032    0.3183    0.3553    0.0199    0.0026    0.0167    0.0293    0.0005];
media(3,:) = [     6.5035    0.8219    0.6994    0.7367    0.6241    0.3071    4.8707    0.9168    0.2044    0.7684    0.5401    0.2505];
dp(3,:)    = [     0.0044    0.0130    0.0012    0.0144    0.0046    0.3304    0.1499    0.0246    0.0078    0.0219    0.0462    0.0007];
%% Algumas op��es
plotar = 2;        % Plota as compara��es caso = 1
n = 3;             % Numero de matrizes
m = length(media); % Numero de colunas
cima = zeros(n,m);
baixo= zeros(n,m);
for i = 1:n
    cima(i,:) = media(i,:) + 2*dp(i,:);
    baixo(i,:)= media(i,:) - 2*dp(i,:);
end
%% Retorno
B = -max(-baixo);
C = max(cima); 
mm = mean(media);
des = std(media); 
disp('===================================================================');
fprintf('XVmin = [');
disp(B);disp(']');
fprintf('XVmax = [');
disp(C);disp(']');
disp('===================================================================');
fprintf('Media das medias = ')
disp (mm);
fprintf('Desvio das medias = ')
disp(des);
 %% Plotar 
 if plotar >= 1 && plotar < 3  
     var = (1:m);        
     
     subplot(3,4,[1 5]);
     k = 1;
     plot(var(k)-.25,media(1,k),'b*',var(k),media(2,k),'b*',var(k)+.25,media(3,k),'b*');
     hold on; grid on; grid minor;
     plot(var(k)-.25,cima(1,k),'g*',var(k),cima(2,k),'g*',var(k)+.25,cima(3,k),'g*');
     plot(var(k)-.25,baixo(1,k),'g*',var(k),baixo(2,k),'g*',var(k)+.25,baixo(3,k),'g*');
        if plotar == 2
        plot([var(k)-0.5 var(k)+0.5],B(k)*ones(1,2),'-r',[var(k)-0.5 var(k)+0.5],C(k)*ones(1,2),'-r')
        end
     xlim([var(k)-0.5 var(k)+0.5]);
     title('Alongamento')

     subplot(3,4,2)
     k = [2 3];
     plot(var(k)-.25,media(1,k),'b*',var(k),media(2,k),'b*',var(k)+.25,media(3,k),'b*');
     hold on; grid on; grid minor;
     plot(var(k)-.25,cima(1,k),'g*',var(k),cima(2,k),'g*',var(k)+.25,cima(3,k),'g*');
     plot(var(k)-.25,baixo(1,k),'g*',var(k),baixo(2,k),'g*',var(k)+.25,baixo(3,k),'g*');
     xlim([1.5 3.5])
     title('Tamanhos das se��es')

     subplot(3,4,3)
     k = [4 5];
     plot(var(k)-.25,media(1,k),'b*',var(k),media(2,k),'b*',var(k)+.25,media(3,k),'b*');
     hold on; grid on; grid minor;
     plot(var(k)-.25,cima(1,k),'g*',var(k),cima(2,k),'g*',var(k)+.25,cima(3,k),'g*');
     plot(var(k)-.25,baixo(1,k),'g*',var(k),baixo(2,k),'g*',var(k)+.25,baixo(3,k),'g*');
     xlim([3.5 5.5])
     title('Afilamentos')

     subplot(3,4,6)
     k = 6;
     plot(var(k)-.25,media(1,k),'b*',var(k),media(2,k),'b*',var(k)+.25,media(3,k),'b*');
     hold on; grid on; grid minor;
     plot(var(k)-.25,cima(1,k),'g*',var(k),cima(2,k),'g*',var(k)+.25,cima(3,k),'g*');
     plot(var(k)-.25,baixo(1,k),'g*',var(k),baixo(2,k),'g*',var(k)+.25,baixo(3,k),'g*');
        if plotar == 2
        plot([var(k)-0.5 var(k)+0.5],B(k)*ones(1,2),'-r',[var(k)-0.5 var(k)+0.5],C(k)*ones(1,2),'-r')
        end
     xlim([var(k)-0.5 var(k)+0.5])
     title('Incid�ncia')

     subplot(3,4,4)
     k = 7;
     plot(var(k)-.25,media(1,k),'b*',var(k),media(2,k),'b*',var(k)+.25,media(3,k),'b*');     
     hold on; grid on; grid minor;
     plot(var(k)-.25,cima(1,k),'g*',var(k),cima(2,k),'g*',var(k)+.25,cima(3,k),'g*');      
     plot(var(k)-.25,baixo(1,k),'g*',var(k),baixo(2,k),'g*',var(k)+.25,baixo(3,k),'g*');  
        if plotar == 2
        plot([var(k)-0.5 var(k)+0.5],B(k)*ones(1,2),'-r',[var(k)-0.5 var(k)+0.5],C(k)*ones(1,2),'-r')
        end
     xlim([var(k)-0.5 var(k)+0.5]);
     title('Perfil')

     
     subplot(3,4,7)
     k = 8;
     plot(var(k)-.25,media(1,k),'b*',var(k),media(2,k),'b*',var(k)+.25,media(3,k),'b*');
     hold on; grid on; grid minor;
     plot(var(k)-.25,cima(1,k),'g*',var(k),cima(2,k),'g*',var(k)+.25,cima(3,k),'g*');
     plot(var(k)-.25,baixo(1,k),'g*',var(k),baixo(2,k),'g*',var(k)+.25,baixo(3,k),'g*');
        if plotar == 2
        plot([var(k)-0.5 var(k)+0.5],B(k)*ones(1,2),'-r',[var(k)-0.5 var(k)+0.5],C(k)*ones(1,2),'-r')
        end
     xlim([var(k)-0.5 var(k)+0.5])
     title('Envergadura EH')
     
     subplot(3,4,8)
     k = 9;
     plot(var(k)-.25,media(1,k),'b*',var(k),media(2,k),'b*',var(k)+.25,media(3,k),'b*');
     hold on; grid on; grid minor;
     plot(var(k)-.25,cima(1,k),'g*',var(k),cima(2,k),'g*',var(k)+.25,cima(3,k),'g*');
     plot(var(k)-.25,baixo(1,k),'g*',var(k),baixo(2,k),'g*',var(k)+.25,baixo(3,k),'g*');
        if plotar == 2
        plot([var(k)-0.5 var(k)+0.5],B(k)*ones(1,2),'-r',[var(k)-0.5 var(k)+0.5],C(k)*ones(1,2),'-r')
        end
     xlim([var(k)-0.5 var(k)+0.5])
     title('Corda EH')
     
     subplot(3,4,9)
     k = 10;
     plot(var(k)-.25,media(1,k),'b*',var(k),media(2,k),'b*',var(k)+.25,media(3,k),'b*');
     hold on; grid on; grid minor;
     plot(var(k)-.25,cima(1,k),'g*',var(k),cima(2,k),'g*',var(k)+.25,cima(3,k),'g*');
     plot(var(k)-.25,baixo(1,k),'g*',var(k),baixo(2,k),'g*',var(k)+.25,baixo(3,k),'g*');
        if plotar == 2
        plot([var(k)-0.5 var(k)+0.5],B(k)*ones(1,2),'-r',[var(k)-0.5 var(k)+0.5],C(k)*ones(1,2),'-r')
        end
     xlim([var(k)-0.5 var(k)+0.5])
     title('LE')
     
     subplot(3,4,10)
     k = 11;
     plot(var(k)-.25,media(1,k),'b*',var(k),media(2,k),'b*',var(k)+.25,media(3,k),'b*');
     hold on; grid on; grid minor;
     plot(var(k)-.25,cima(1,k),'g*',var(k),cima(2,k),'g*',var(k)+.25,cima(3,k),'g*');
     plot(var(k)-.25,baixo(1,k),'g*',var(k),baixo(2,k),'g*',var(k)+.25,baixo(3,k),'g*');
        if plotar == 2
        plot([var(k)-0.5 var(k)+0.5],B(k)*ones(1,2),'-r',[var(k)-0.5 var(k)+0.5],C(k)*ones(1,2),'-r')
        end
     xlim([var(k)-0.5 var(k)+0.5])
     title('Altura EH')
     
     subplot(3,4,11)
     k = m;
     plot(var(k)-.25,media(1,k),'b*',var(k),media(2,k),'b*',var(k)+.25,media(3,k),'b*');
     hold on; grid on; grid minor;
     plot(var(k)-.25,cima(1,k),'g*',var(k),cima(2,k),'g*',var(k)+.25,cima(3,k),'g*');
     plot(var(k)-.25,baixo(1,k),'g*',var(k),baixo(2,k),'g*',var(k)+.25,baixo(3,k),'g*');
        if plotar == 2
        plot([var(k)-0.5 var(k)+0.5],B(k)*ones(1,2),'-r',[var(k)-0.5 var(k)+0.5],C(k)*ones(1,2),'-r')
        end
     xlim([var(k)-0.5 var(k)+0.5])
     title('CG')
 end     

if plotar == 3
var = categorical({'ARw', 'pb_1', 'pb_2', '\lambda_{2}', '\lambda_{3}', 'ai','perfil','B_{eh}', 'c_{eh}', 'LE', 'altura', 'CG'});
var = reordercats(var,{'ARw', 'pb_1', 'pb_2', '\lambda_{2}', '\lambda_{3}', 'ai','perfil','B_{eh}', 'c_{eh}', 'LE', 'altura', 'CG'});

figure(2)
subplot(2,1,1)
media = media';
bar(media)
title('Media')
grid on; grid minor;
subplot(2,1,2)
dp = dp';
bar(dp)
title('Desvio padrao')
grid on; grid minor;
end
