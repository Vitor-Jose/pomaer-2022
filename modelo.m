function [result] = modelo(x,~,individuo)

% MODELO DE OTIMIZACAO DA AERONAVE MONOPLANO 2022
% TUCANO 2022 - REGULAR

fprintf('Aeronave n: %d; Iter: %d; Ind: %d\n', individuo(3), individuo(1), individuo(2))
disp('Vetor x: '); disp(mat2str(x));disp('');

%% -------------------------- INICIALIZACAO -------------------------------

tic;                % inicio da contagem de tempo
sim.paralelo  = 1;  % Indica se usa ou não processamento paralelo
                    % OBS: Caso rodar paralelo, pre-definir o numero de
                    % processadores pelo comando parpool(n)
                    % Ex: 3 processadores - parpool(3)
                    
sim.panel    = 15;  % [-] Numero parametrizado de paineis VLM
sim.intermax = 30;  % [-] Numero de iteracoes maxima no VLM
                    
sim.dist     = 0;   % [-] Comando para retirar alpha efetivo do VLM (usado apenas para o estol...)
sim.npro     = 4;   % [-] Caso paralelo, quantos processamentos de uma vez no estol
sim.ajuste   = 1;   % [-] Uso de curvas polinomiais ou interpoladas

% FLIGTH CONDITIONS
[flc] = condicoesVOO();
flc.Vrot = x(8);
flc.Voo  = x(8);

% 0.4536
aeronave.asa1    = [2 x(1) x(4) x(3) floor(5) floor(5) floor(x(2)) x(5)];
% ['Envergadura' 'Alongamento' 'Estacao central' 'Afilamento da ponta' 'Perfil1' 'Perfil2' 'Perfil3' 'Incidencia] 
aeronave.incid   = [0 0 0];
% Respectivas incidencias
aeronave.eh      = [x(6) 0.20 floor(20) 0 1 0.32 1.3];
% ['Envergadura' 'Corda' 'Perfil' 'Incidencia' 'Efetividade' 'Altura[metros]' 'LE'];
aeronave.ev      = [0.2 0.00 10];
% ['Envergadura da ev' 'Posicao do centro em relacao ao CA da EH' 'Perfil EV'];
aeronave.outros  = [0.045 0 5 0.11 0.15];
% ['Posicao do CG [m]' 'Posicao vertical do CG [m] ' 'Grupo moto-propulsor' 'Posicao vertical do motor]' ''Altura do TP';

% GEOMETRICO
[geo,penal]     = geometrico(aeronave,sim);

% PLOTAR (0: Nao plota; 1: Distribuicao de paineis; 2: Geometria)
% VLManda(geo,flc,sim,2,'-LiftingSurfaces');% So colar na command Window
%% ------------------------- AERODINAMICA ---------------------------------
% Calculo do estol
if ~penal
    if sim.paralelo 
        [ard,geo]               = estolp(geo, flc, sim);

    else 
        [ard,geo]               = estol(geo, flc, sim);
    end
    ard.def = ard.estoleh + x(7);
% save('ard','ard','geo')
% load('ard')

%% --- LE com base no �ngulo de estol ---

%% -------------------- ESTABILIDADE E CONTROLE ---------------------------
[penal,stab]         = estabilidade(geo,flc,sim,ard);
% save('stab','stab');
% load('stab')
end

%% ------------------------- DESEMPENHO -----------------------------------
if ~penal
    [mtow]   		= dinamica(geo, flc, sim, ard, stab);

%% ------------------------- ESTRUTURAS -----------------------------------
    [pv] 			= EstimaMassa(geo, mtow);
else
    mtow = 5;
    pv = 3;
end
%% -------------------------- PONTUACAO -----------------------------------
cp = mtow - pv; 

[Pontuacao] 		= pontuacao(cp, pv, 154, 45);
result              = -Pontuacao;

tempo = toc;
%% --------------------------- DISPLAY ------------------------------------
disp('');
fprintf('MTOW = %f\n', mtow);
fprintf('PV = %f\n', pv);
fprintf('PONTOS = %f\n', Pontuacao);
disp('');
fprintf('TEMPO = %.3f\n',tempo);
disp('===================================================================');
% save('Aeronave.mat')